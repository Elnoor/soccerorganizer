﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SoccerOrganizer.Models.Entities;

namespace SoccerOrganizer.Controllers
{
    public class AccountController : BaseController
    {
        public AccountController(UserManager<User> userManager) : base(userManager)
        {
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            User currentUser = await GetCurrentUserAsync();

            if (currentUser == null)
            {
                return Ok(new { success = false, errors = new List<string> { "User not found" } });

                //better user
                //return BadRequest();
            }

            var roles = await _userManager.GetRolesAsync(currentUser);

            return Ok(new
            {
                success = true,
                data = new
                {
                    Id = currentUser.Id,
                    FirstName = currentUser.FirstName,
                    LastName = currentUser.LastName,
                    Email = currentUser.Email,
                    Roles = roles
                }
            });
        }
    }
}