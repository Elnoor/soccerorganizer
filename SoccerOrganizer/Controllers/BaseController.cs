﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SoccerOrganizer.Models.Entities;

namespace SoccerOrganizer.Controllers
{
    [Route("Api/[controller]")]
    [Authorize]
    public class BaseController : Controller
    {
        protected readonly UserManager<User> _userManager;

        public BaseController() { }

        // GetCurrentUserAsync() method won't work without userManager. So in a controller that currentUser will be accessed make sure there is userManager
        public BaseController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        protected async Task<User> GetCurrentUserAsync()
        {
            return await _userManager.GetUserAsync(User);
        }
    }
}