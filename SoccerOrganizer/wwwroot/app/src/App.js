import React from "react";
import logo from "./img/logo.svg";
import "./css/App.css";
import Account from "./js/Components/Account";
import axios from "axios";

class App extends React.Component {
  constructor(props) {
    super(props);

    // Authentication won't work without this. This will add auth cookies to requests.
    axios.defaults.withCredentials = true;
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Account />

          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

export default App;
