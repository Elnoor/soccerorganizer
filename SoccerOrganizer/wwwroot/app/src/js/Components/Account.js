import React from "react";
import axios from "axios";
import {URLS} from './../Constants/Urls';

export default class Account extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      currentuser: null,
      errors: []
    };
  }

  componentDidMount() {
    const root = this;

    axios
      .get(URLS.ACCOUNT.BASE)
      .then(function(response) {
        if (response.data.success && response.data.data) {
          root.setState({ currentUser: response.data.data });
        } else {
          if (response.data.errors)
            root.setState({ errors: response.data.errors });
        }
      })
      .catch(function(response) {
        root.setState({ errors: [response.message] });
      })
      .then(function() {
        root.setState({ loading: false });
      });
  }

  render() {
    if (this.state.loading) {
      return <p>Loading...</p>;
    }
    if (this.state.errors && this.state.length > 0) {
      return (
        <ul>
          {this.state.errors.map((error, index) => (
            <li key={index}>{error}</li>
          ))}
        </ul>
      );
    }
    return (
      <div>
        Hello, <br/>You are currently logged in as:
        <p>{this.state.currentUser.firstName}</p>
        <p>{this.state.currentUser.lastName}</p>
        <p>{this.state.currentUser.email}</p>
      </div>
    );
  }
}
