var root = window.location.hostname === "localhost" ? "https://localhost:44354" : window.location.origin;

let urls = {
  ACCOUNT: {
    BASE: "Account",
    SIGN_IN: "Account/Signin",
    SIGN_UP: "Account/Signup"
  },
  PLAYERS: {
    BASE: "Players",
    TOP_UP: "Players/TopUp"
  }
};

function addRootToUrls(obj) {
  Object.entries(obj).forEach(function([key, value]) {
    if (value) {
      if (typeof value === "object") return addRootToUrls(value);
      else obj[key] = `${root}/Api/${value}/`;
    }
  });
  return obj;
}

export const URLS = addRootToUrls(urls);
